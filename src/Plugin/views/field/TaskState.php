<?php

namespace Drupal\community_tasks\Plugin\views\field;
use Drupal\views\Plugin\views\field\Markup;

/**
 * Views field handler
 */
class TaskState extends Markup {

  function query() {
    $this->ensure_my_table();
    $this->additional_fields['nid'] = ['table' => 'node', 'field' => 'nid'];
  }

  function render($row) {
    return [
      '#type' => 'community_task_state',
      '#nid' => $row->nid
    ];
  }
}