<?php

/**
 * @file
 */

use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Entity\Entity\EntityViewDisplay;

/**
 * Implements hook_install().
 *
 * Check if there is a node comments field, and if not, create one
 */
function community_tasks_install() {
  //for better compatibility with other modules try to re-use existing fields
  community_tasks_reuse_node_field_storage('body');
  FieldConfig::Create([
    'field_name' => 'body',
    'entity_type' => 'node',
    'field_type' => 'text_with_summary',
    'bundle' => 'community_task',
    'label' => t('Details'),
  ])->save();

  community_tasks_reuse_node_field_storage('node_comments');
  FieldConfig::Create([
    'field_name' => 'node_comments',
    'entity_type' => 'node',
    'field_type' => 'comment',
    'bundle' => 'community_task',
    'label' => t('Comments'),
  ])->save();

  //having added these late, we need to manually enable these fields in the entity_view_displays
  $body_options = [
    'type' => 'text_default',
    'weight' => 0,
    'label' => 'hidden'
  ];
  $comments_options = [
    'type' => 'comment_default',
    'weight' => 4,
    'label' => 'hidden',
    'settings' => ['pager_id' => 0]
  ];
  EntityViewDisplay::load('node.community_task.teaser')
    ->setComponent('body', $body_options)
    ->save();

  EntityViewDisplay::load('node.community_task.default')
    ->setComponent('body', $body_options)
    ->setComponent('node_comments', $comments_options)
    ->save();
}

function community_tasks_reuse_node_field_storage($field_name) {
  $storage_ids = \Drupal::entityQuery('field_storage_config')
    ->condition('field_name', $field_name)
    ->condition('entity_type', 'node')
    ->execute();
  $storage_id = reset($storage_ids);
  if ($storage_id) {
    return;
  }
  switch($field_name) {
    case 'body':
      $props = [
        'type' => 'text_with_summary',
        'settings' => [
          'datetime_type' =>  'datetime'
        ],
      ];
      break;
    case 'node_comments':
      $props = [
        'type' => 'comment',
        'settings' => [
          'comment_type' => 'node'
        ],
      ];
  }
  $shared = [
    'field_name' => $field_name,
    'entity_type' => 'node',
    'provider' => 'community_tasks',
    'cardinality' => 1
  ];
  $field = FieldStorageConfig::Create($props+$shared);
  $field->save();
}
